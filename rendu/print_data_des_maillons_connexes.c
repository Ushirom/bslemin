#include "graph.h"

void        print_data_des_maillons_connexes(t_maillon *maillon) {
    int     i = -1;

    while (maillon->linked[++i] != 0)
        print_maillon(maillon->linked[i]);
}
