#include <stdlib.h>
#include "graph.h"

void        link_maillons(t_maillon *maillon1, t_maillon *maillon2) {
    if ((maillon1->linked = realloc(maillon1->linked,
                    sizeof(t_maillon*) * (maillon1->size + 2))) == NULL)
        return ;
    maillon1->linked[maillon1->size++] = maillon2;
    maillon1->linked[maillon1->size] = NULL;
}
