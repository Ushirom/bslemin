#include "graph.h"

void        print_my_graph_data(t_maillon *maillon) {
    int i = -1;

    print_maillon(maillon);
    while (maillon->linked[++i]) {
        print_my_graph_data(maillon->linked[i]);
    }
}
