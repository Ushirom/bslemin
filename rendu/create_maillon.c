#include <stdlib.h>
#include "graph.h"

t_maillon       *create_maillon(int data) {
    t_maillon   *tmp;

    if ((tmp = malloc(sizeof(t_maillon))) == NULL)
        return NULL;
    tmp->data = data;
    if ((tmp->linked = malloc(sizeof(t_maillon*) * 1)) == NULL)
        return NULL;
    tmp->linked[0] = NULL;
    tmp->size = 0;
    return tmp;
}


