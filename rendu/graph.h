#ifndef GRAPH_H_
# define GRAPH_H_

typedef struct s_maillon {
    int                 data;
    struct s_maillon   **linked;
    long               size;
}                       t_maillon;


t_maillon       *create_maillon(int data);
void            print_maillon(t_maillon *maillon);
void            link_maillons(t_maillon *maillon1, t_maillon *maillon2);
void            print_data_des_maillons_connexes(t_maillon *maillon);
t_maillon       *build_my_graph();
void            print_my_graph_data(t_maillon*);

#endif
