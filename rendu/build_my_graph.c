#include "graph.h"

t_maillon   *build_my_graph() {
     t_maillon  *root;
     t_maillon  *m60012, *m30012, *m48,* m50012, *m3, *m8, *m98;

     root = create_maillon(42);
     m60012 = create_maillon(60012);
     m30012 = create_maillon(30012);
     m48 = create_maillon(48);
     m50012 = create_maillon(50012);
     m3 = create_maillon(3);
     m8 = create_maillon(8);
     m98 = create_maillon(98);
     link_maillons(root, m60012);
     link_maillons(m60012, m30012);
     link_maillons(root, m48);
     link_maillons(m48, m50012);
     link_maillons(m50012, m3);
     link_maillons(m3, m98);
     link_maillons(m3, m8);
     return root;
}
