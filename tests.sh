#!/bin/sh

alias gcc="gcc -I./rendu -Wall -Wextra"

gcc -o etape0 mains/part1etape0.c rendu/create_maillon.c
./etape0
if [ "$?" = "0" ]
then
    echo "You have passed the etape 0 !"
else
    echo "You have failed the etape 0 !"
    exit 1
fi

gcc -o etape1 mains/part1etape1.c rendu/create_maillon.c rendu/print_maillon.c
./etape1 > tmp/etape1_your_output
diff tmp/etape1_your_output tmp/etape1_good_output
if [ "$?" = "0" ]
then
    echo "You have passed the etape 1 !"
else
    echo "You have failed the etape 1 !"
    exit 1
fi

gcc -o etape3 mains/part1etape3.c rendu/create_maillon.c rendu/print_maillon.c rendu/link_maillons.c rendu/print_data_des_maillons_connexes.c
./etape3 | sort > tmp/etape3_your_output
diff tmp/etape3_your_output tmp/etape3_good_output
if [ "$?" = "0" ]
then
    echo "You have passed the etape 3 !"
else
    echo "You have failed the etape 3 !"
    exit 1
fi

gcc -o etape5 mains/part1etape5.c rendu/create_maillon.c rendu/print_maillon.c rendu/link_maillons.c rendu/print_data_des_maillons_connexes.c rendu/build_my_graph.c rendu/print_my_graph_data.c
./etape5 | sort > tmp/etape5_your_output
diff tmp/etape5_your_output tmp/etape5_good_output
if [ "$?" = "0" ]
then
    echo "You have passed the etape 5 !"
else
    echo "You have failed the etape 5 !"
    exit 1
fi

echo "You have passed all the bootstrap !"
