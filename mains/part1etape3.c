#include <stdio.h>
#include "graph.h"

int main() {
    t_maillon *m1 = create_maillon(42);
    t_maillon *m2 = create_maillon(888);
    t_maillon *m3 = create_maillon(789);

    link_maillons(m1, m2);
    link_maillons(m1, m3);

    print_data_des_maillons_connexes(m1);
    print_data_des_maillons_connexes(m2);
    print_data_des_maillons_connexes(m3);
    return 0;
}
