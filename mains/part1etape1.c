#include "graph.h"

int         main() {
    t_maillon   *m1 = create_maillon(42);
    t_maillon   *m2 = create_maillon(888);

    print_maillon(m1);
    print_maillon(m2);
    return 0;
}
