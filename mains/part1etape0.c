#include <stdio.h>
#include "graph.h"

int         main() {
    t_maillon   *m1 = create_maillon(42);
    t_maillon   *m2 = create_maillon(888);

    if (m1->data != 42 ||
        m2->data != 888)
    {
        fprintf(stderr, "Your data is not correctly set\n");
        return 1;
    }
    return 0;
}
